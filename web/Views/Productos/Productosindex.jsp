<%-- 
    Document   : Home
    Created on : 24-11-2018, 1:15:32
    Author     : Diego
--%>
<%@taglib uri="/struts-tags" prefix="s" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="">

    <title>Modulo Productos</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/admin.css" rel="stylesheet">
  </head>

  <body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">TDA Punto Ventas</a>
      <input class="form-control form-control-dark w-30" 
             type="text" placeholder="Buscar Producto" aria-label="Buscar Productos">
      <ul class="navbar-nav px-3 ">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="#">Cerrar Sesion</a>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="home"></span>
                  Inicio 
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="#">
                  <span data-feather="archive"></span>
                  Modulo Productos
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="compass"></span>
                  Modulo Sucursales
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="validar">
                  <span data-feather="users"></span>
                  Modulo Usuarios
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="bar-chart-2"></span>
                  Reportes
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="dollar-sign"></span>
                  Detalles de Ventas
                </a>
              </li>
            </ul>

              
          
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="text-center">Modulo Productos</h1>
            
          </div>
          
          <div class="row">
              
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                Listado Productos  
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                  <table class="table table-striped table-sm">
                                    <thead>
                                      <tr>
                                        <th>Id</th>
                                        <th>Nombre</th>
                                        <th>Stock Minimo</th>
                                        <th>Categoria</th>
                                        <th>Editar</th>
                                        <th>Deshabilitar</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        <s:iterator value="ListaPro" var="ListaPro">

                                      <tr>
                                        <td><s:property value="id" /></td>
                                        <td><s:property value="nombre" /></td>
                                        <td><s:property value="stockMinimo" /></td>
                                        <td><s:property value="categorias.nombre"/></td>
                                        <td><s:a action="Editar">
                                                <span data-feather="edit-2"></span>
                                            </s:a>
                                        </td>
                                        <td>
                                            <s:a action="Deshabilitar">
                                                <span data-feather="trash"></span>
                                            </s:a>
                                        </td>
                                      </tr>
                                        </s:iterator>
                                    </tbody>
                                  </table>
                                </div>
                            </div>
                        </div>
                    </div>
              
              <div class="col-sm-6">
                  <div class="card">
                    <div class="card-header">
                      Agregar un Producto
                    </div>
                    <div class="card-body">
                        <s:form action="Save" method="POST" cssClass="form">
                        <s:div cssClass="form-group">
                          <s:textfield label="Nombre" name="pro.nombre" cssClass="form-control" />
                        </s:div>
                        <s:div cssClass="form-group">
                          <s:textfield label="Stock Minimo" name="pro.stockMinimo" cssClass="form-control" />
                        </s:div>
                        <s:div cssclass="form-group">
                            <s:select label="Categoria" list="ListaCat" listValue="nombre" listKey="id" name="Categoria_id" cssClass="btn btn-success btn-sm"/>
                        </s:div>
                            <s:submit cssClass="btn btn-success" value="Guardar Producto" />
                      </s:form>
                    
                    </div>
                  </div>
              </div>
          </div>
        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace()
    </script>

  
    
  </body>
</html>