/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Map;
import modelos.Categorias;
import modelos.dao.CategoriasDao;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author Diego
 */
public class CategoriasController extends ActionSupport implements SessionAware{
    
    private Map<String,Object> session;
    public Categorias cat;
    public CategoriasDao catdao;
    public ArrayList<Categorias> ListaCat;

    public Categorias getCat() {
        return cat;
    }

    public void setCat(Categorias cat) {
        this.cat = cat;
    }

    public CategoriasDao getCatdao() {
        return catdao;
    }

    public void setCatdao(CategoriasDao catdao) {
        this.catdao = catdao;
    }

    public ArrayList<Categorias> getListaCat() {
        return ListaCat;
    }

    public void setListaCat(ArrayList<Categorias> ListaCat) {
        this.ListaCat = ListaCat;
    }
    
    //metodos exclusivos para cargar la lista respectiva para ser leida desde otro controlador
    
    public void CargarCategorias(){
        
       catdao = new CategoriasDao();
       this.setListaCat(catdao.getAll());

    }
    
    public Categorias BuscarCategoriaId(int id){
        catdao = new CategoriasDao();
        Categorias cate = catdao.BuscarPorId(id);
        return cate;
    }
    
    
    @Override
    public void setSession(Map<String,Object> session){
    
    this.session = session;
    }
    
}
