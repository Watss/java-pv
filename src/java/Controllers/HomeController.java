/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Map;
import modelos.Categorias;
import modelos.Productos;
import modelos.dao.ProductosDao;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author Diego
 */
public class HomeController extends ActionSupport implements SessionAware {
    public Productos pro;
    public ProductosDao prodao;
    public ArrayList<Productos> ListaPro;
    public ArrayList<Categorias> ListaCat;
    private Map<String,Object> session;

    public Productos getPro() {
        return pro;
    }

    public void setPro(Productos pro) {
        this.pro = pro;
    }

    public ProductosDao getProdao() {
        return prodao;
    }

    public void setProdao(ProductosDao prodao) {
        this.prodao = prodao;
    }

    public ArrayList<Productos> getListaPro() {
        return ListaPro;
    }

    public void setListaPro(ArrayList<Productos> ListaPro) {
        this.ListaPro = ListaPro;
    }
    
    @Override
    public void setSession(Map<String,Object> session){
    
    this.session = session;
    }
    
    
    @Override
    public String execute(){
        
        return SUCCESS;
    }
    public String Validar(){
        this.prodao = new ProductosDao();
        this.ListaPro = this.prodao.getAll();
        CategoriasController cc = new CategoriasController();
        cc.CargarCategorias();
        this.ListaCat = cc.getListaCat();
        return SUCCESS;
    }

  
}
