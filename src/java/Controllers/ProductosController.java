/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Map;
import modelos.Categorias;
import modelos.Productos;
import modelos.dao.ProductosDao;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author Diego
 */
public class ProductosController extends ActionSupport implements SessionAware {
    
    private Map<String,Object> session;
    public Productos pro;
    public ProductosDao prodao;
    public int Categoria_id;
    public ArrayList<Categorias> ListaCat;
    public ArrayList<Productos> ListaPro;

    public Productos getPro() {
        return pro;
    }

    public void setPro(Productos pro) {
        this.pro = pro;
    }
    
    @Override
    public void setSession(Map<String,Object> session){
    
    this.session = session;
    }

    public int getCategoria_id() {
        return Categoria_id;
    }
    
     @Override
    public String execute(){
        
        this.prodao = new ProductosDao();
        this.ListaPro = this.prodao.getAll();
        CategoriasController cc = new CategoriasController();
        cc.CargarCategorias();
        this.ListaCat = cc.getListaCat();
        
        return SUCCESS;
    }
    public String Save(){
        CategoriasController cc = new CategoriasController();
        Categorias ca = cc.BuscarCategoriaId(Categoria_id);
        pro.setNombre(pro.getNombre());
        pro.setStockMinimo(pro.getStockMinimo());
        pro.setEstado("ON");
        pro.setCategorias(ca);
        boolean status = prodao.GuardarProducto(pro);
        if(status){
            return SUCCESS;
        }
       return ERROR;
    }
    public String Validar(){
        
        return SUCCESS;
    }
    
}
