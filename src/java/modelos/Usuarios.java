package modelos;
// Generated 24-11-2018 21:22:03 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Usuarios generated by hbm2java
 */
public class Usuarios  implements java.io.Serializable {


     private Integer id;
     private Sucursales sucursales;
     private TipoUsuario tipoUsuario;
     private String nombres;
     private String apellidoPaterno;
     private String apellidoMaterno;
     private String rut;
     private String correo;
     private String password;
     private Set ventases = new HashSet(0);

    public Usuarios() {
    }

	
    public Usuarios(Sucursales sucursales, TipoUsuario tipoUsuario, String nombres, String apellidoPaterno, String apellidoMaterno, String rut, String correo, String password) {
        this.sucursales = sucursales;
        this.tipoUsuario = tipoUsuario;
        this.nombres = nombres;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.rut = rut;
        this.correo = correo;
        this.password = password;
    }
    public Usuarios(Sucursales sucursales, TipoUsuario tipoUsuario, String nombres, String apellidoPaterno, String apellidoMaterno, String rut, String correo, String password, Set ventases) {
       this.sucursales = sucursales;
       this.tipoUsuario = tipoUsuario;
       this.nombres = nombres;
       this.apellidoPaterno = apellidoPaterno;
       this.apellidoMaterno = apellidoMaterno;
       this.rut = rut;
       this.correo = correo;
       this.password = password;
       this.ventases = ventases;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public Sucursales getSucursales() {
        return this.sucursales;
    }
    
    public void setSucursales(Sucursales sucursales) {
        this.sucursales = sucursales;
    }
    public TipoUsuario getTipoUsuario() {
        return this.tipoUsuario;
    }
    
    public void setTipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }
    public String getNombres() {
        return this.nombres;
    }
    
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
    public String getApellidoPaterno() {
        return this.apellidoPaterno;
    }
    
    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }
    public String getApellidoMaterno() {
        return this.apellidoMaterno;
    }
    
    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }
    public String getRut() {
        return this.rut;
    }
    
    public void setRut(String rut) {
        this.rut = rut;
    }
    public String getCorreo() {
        return this.correo;
    }
    
    public void setCorreo(String correo) {
        this.correo = correo;
    }
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    public Set getVentases() {
        return this.ventases;
    }
    
    public void setVentases(Set ventases) {
        this.ventases = ventases;
    }




}


