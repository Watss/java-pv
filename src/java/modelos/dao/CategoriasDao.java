/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import modelos.Categorias;
import modelos.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Diego
 */
public class CategoriasDao {
    
    Session session = HibernateUtil.getSessionFactory().openSession();
    Transaction t = null;
    
    public ArrayList<Categorias> getAll(){
        
        
        
        try {
            
            t= session.beginTransaction();
            ArrayList<Categorias> ListaCategorias = new ArrayList<>();
            List<Categorias> lp =  session.createQuery("FROM Categorias").list();
            Iterator<Categorias> iter = lp.iterator();
            t.commit();
            
            while(iter.hasNext()){
                Categorias p =iter.next();
                ListaCategorias.add(p);
            }
           return ListaCategorias;
            
        } catch (HibernateException e) {
            if(t != null){
                
                t.rollback();
            }
            
        }finally{
        session.close();
            
        }
        return null;
    }
    
    public Categorias BuscarPorId(int id){
        
        try {
            t= session.beginTransaction();
            Query q = session.createQuery("FROM Categorias WHERE id = :id ");
            q.setParameter("id",id);
            Categorias c = (Categorias) q.uniqueResult();
            t.commit();
            
            
           return c;
            
        } catch (HibernateException e) {
            if(t != null){
                
                t.rollback();
            }
            
        }finally{
        session.close();
            
        }
        return null;
    }
    
}
