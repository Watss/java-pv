/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import modelos.HibernateUtil;
import modelos.Productos;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Diego
 */
public class ProductosDao {

    Session session = HibernateUtil.getSessionFactory().openSession();
    Transaction t = null;
    public ArrayList<Productos> getAll(){
        
        
        
        try {
            
            t= session.beginTransaction();
            ArrayList<Productos> ListaProductos = new ArrayList<>();
            List<Productos> lp =  session.createQuery("FROM Productos").list();
            Iterator<Productos> iter = lp.iterator();
            t.commit();
            
            while(iter.hasNext()){
                
                Productos p =iter.next();
                Hibernate.initialize(p.getCategorias());
                ListaProductos.add(p);
            }
           return ListaProductos;
            
        } catch (HibernateException e) {
            if(t != null){
                
                t.rollback();
            }
            
        }finally{
        session.close();
            
        }
        return null;
    }
    
    public boolean GuardarProducto(Productos pro){
       
        try {
            t = session.beginTransaction();
            session.save(pro);
            t.commit();
            return true;
        } catch (HibernateException e) {
            if(t != null){
            t.rollback();
            }
        }finally{
            session.close();
        }
        return false;
    }
    public boolean ActualizarProducto(Productos pro){
       
        try {
            t = session.beginTransaction();
            session.update(pro);
            t.commit();
            return true;
        } catch (HibernateException e) {
            if(t != null){
            t.rollback();
            }
        }finally{
            session.close();
        }
        return false;
    }
    public boolean DeshabilitarProducto(int id){
       String sql = "Update Productos set estado = OFF where id = :id";
        try {
            t = session.beginTransaction();
            Query q = session.createSQLQuery(sql);
            q.setInteger("id", id);
            q.executeUpdate();
            t.commit();
            return true;
        } catch (HibernateException e) {
            if(t != null){
            t.rollback();
            }
        }finally{
            session.close();
        }
        return false;
    }
    
}
